import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {

    private List<Employee> team;

    public Manager(String firstName, String lastName, double salary, int experience, Manager manager) {
        super(firstName, lastName, salary, experience, manager);
        this.team = new ArrayList<>();
    }

    public List<Employee> getTeam() {
        return team;
    }

    public void setTeam(List<Employee> team) {
        this.team = team;
    }

    @Override
    public double countSalary() {
        if (team.size() > 10) {
            return countSalaryByTeamStructure() + 300;
        } else if (team.size() > 5) {
            return countSalaryByTeamStructure() + 200;
        } else {
            return countSalaryByTeamStructure();
        }
    }

    private double countSalaryByTeamStructure() {
        if (compareAmountDevelopersAndDesigners() > 0) {
            return super.countSalary() + this.getSalary() * 0.1;
        } else {
            return super.countSalary();
        }
    }

    private int compareAmountDevelopersAndDesigners() {
        int count = 0;
        for (Employee e : team) {
            if (e instanceof Developer) {
                count++;
            } else {
                count--;
            }
        }
        return count;
    }
}
