public class Developer extends Employee {

    public Developer(String firstName, String lastName, double salary, int experience, Manager manager) {
        super(firstName, lastName, salary, experience, manager);
    }
}
