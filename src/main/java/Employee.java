import java.util.Objects;

public abstract class Employee {

    private String firstName;
    private String lastName;
    private double salary;
    private int experience;
    private Manager manager;

    public Employee(String firstName, String lastName, double salary, int experience, Manager manager) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.experience = experience;
        this.manager = manager;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public double countSalary() {
        if (experience > 5) {
            return salary + salary * 0.2 + 500;
        } else if (experience > 2) {
            return salary + 200;
        } else {
            return salary;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Double.compare(employee.salary, salary) == 0 &&
                experience == employee.experience &&
                firstName.equals(employee.firstName) &&
                lastName.equals(employee.lastName) &&
                Objects.equals(manager, employee.manager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, salary, experience, manager);
    }

    @Override
    public String toString() {
        return "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", manager=" + manager.getLastName() +
                ", experience=" + experience;
    }
}