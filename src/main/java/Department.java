import java.util.ArrayList;
import java.util.List;

public class Department {
    private List<Manager> managers;

    public Department() {
        managers = new ArrayList<>();
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public void giveSalary() {
        for (Manager m : managers) {
            System.out.println(m.getFirstName() + " " + m.getLastName() + ": got salary: " + m.countSalary());
            for (Employee e : m.getTeam()) {
                System.out.println(e.getFirstName() + " " + e.getLastName() + ": got salary: " + e.countSalary());
            }
        }
    }
}
