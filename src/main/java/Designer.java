public class Designer extends Employee {
    private double coefficient;

    public Designer(String firstName, String lastName, double salary, int experience, Manager manager, double coefficient) {
        super(firstName, lastName, salary, experience, manager);
        this.coefficient = coefficient;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        if (coefficient < 0 && coefficient > 1) {
            System.out.println("Coefficient must be in range: 0 < coefficient <= 1");
        } else {
            this.coefficient = coefficient;
        }
    }

    @Override
    public double countSalary() {
        if (this.getExperience() > 5) {
            return this.getSalary() * coefficient + this.getSalary() * 0.2 + 500;
        } else if (this.getExperience() > 2) {
            return this.getSalary() * coefficient + 200;
        } else {
            return this.getSalary() * coefficient;
        }
    }
}
