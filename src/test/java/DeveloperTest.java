import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeveloperTest {
    Manager manager;
    Developer developer;

    @BeforeEach
    void init() {
        developer = new Developer("FirstName", "LastName", 100.0, 1, manager);
    }

    @Test
    void countSalaryIfExperienceLessThanTwoYears() {
        developer.setExperience(1);
        assertEquals(100, developer.countSalary());
    }

    @Test
    void countSalaryIfExperienceMoreThanTwoAndLessThanFiveYears() {
        developer.setExperience(4);
        assertEquals(300, developer.countSalary());
    }

    @Test
    void countSalaryIfExperienceMoreThanFiveYears() {
        developer.setExperience(6);
        assertEquals(620, developer.countSalary());
    }


}