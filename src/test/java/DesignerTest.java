import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DesignerTest {
    Designer designer;
    Manager manager;

    @BeforeEach
    void init() {
        designer = new Designer("FirstName", "LastName", 100, 1, manager, 0.5);
    }

    @Test
    void testCountSalaryIfCoefficientIsZeroAndExperienceIsLessThanTwoYears() {
        designer.setCoefficient(0);
        assertEquals(0, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientIsZeroAndExperienceIsMoreThanTwoAndLessThanFiveYears() {
        designer.setCoefficient(0);
        designer.setExperience(3);
        assertEquals(200, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientIsZeroAndExperienceIsMoreThanFiveYears() {
        designer.setCoefficient(0);
        designer.setExperience(7);
        assertEquals(520, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientMoreThanZeroAndLessThanOneAndExperienceIsLessThanTwoYears() {
        designer.setCoefficient(0.5);
        designer.setExperience(1);
        assertEquals(50, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientMoreThanZeroAndLessThanOneAndExperienceIsMoreThanTwoAndLessThanFiveYears() {
        designer.setCoefficient(0.5);
        designer.setExperience(3);
        assertEquals(250, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientMoreThanZeroAndLessThanOneAndExperienceIsMoreThanFiveYears() {
        designer.setCoefficient(0.5);
        designer.setExperience(7);
        assertEquals(570, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientIsOneAndExperienceIsLessThanTwoYears() {
        designer.setCoefficient(1);
        designer.setExperience(1);
        assertEquals(100, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientIsOneAndExperienceIsMoreThanTwoAndLessThanFiveYears() {
        designer.setCoefficient(1);
        designer.setExperience(3);
        assertEquals(300, designer.countSalary());
    }

    @Test
    void testCountSalaryIfCoefficientIsOneAndExperienceIsMoreThanFiveYears() {
        designer.setCoefficient(1);
        designer.setExperience(7);
        assertEquals(620, designer.countSalary());
    }


}