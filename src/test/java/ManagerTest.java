import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ManagerTest {
    Manager manager;
    List<Employee> employees;
    Developer developer1;
    Designer designer1;

    @BeforeEach
    void setUp() {
        manager = new Manager("Manager1", "Mazumbek", 100, 6, null);
        employees = new ArrayList<>();
        developer1 = new Developer("Trrr", "HaHa", 100.11, 2, manager);
        designer1 = new Designer("Trrr", "ChaCha", 100.11, 2, manager, 0.5);
    }

    @Test
    void countSalaryIfTeamSizeLessThanFiveAndTheNumberOfDesignersisMoreOrTheSameAsProgrammers() {
        employees.add(developer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(100, manager.countSalary());
        manager.setExperience(3);
        assertEquals(300, manager.countSalary());
        manager.setExperience(6);
        assertEquals(620, manager.countSalary());
    }

    @Test
    void countSalaryIfTeamSizeLessThanFiveAndTheNumberOfDesignersIsLessThanProgrammers() {
        employees.add(developer1);
        employees.add(developer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(110, manager.countSalary());
        manager.setExperience(3);
        assertEquals(310, manager.countSalary());
        manager.setExperience(6);
        assertEquals(630, manager.countSalary());
    }

    @Test
    void countSalaryIfTeamSizeIsMoreThanFiveAndTheNumberOfDesignersisMoreOrTheSameAsProgrammers() {
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(300, manager.countSalary());
        manager.setExperience(3);
        assertEquals(500, manager.countSalary());
        manager.setExperience(6);
        assertEquals(820, manager.countSalary());
    }

    @Test
    void countSalaryIfTeamSizeIsMoreThanFiveAndTheNumberOfDesignersIsLessThanProgrammers() {
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(310, manager.countSalary());
        manager.setExperience(3);
        assertEquals(510, manager.countSalary());
        manager.setExperience(6);
        assertEquals(830, manager.countSalary());
    }

    @Test
    void countSalaryIfTeamSizeIsMoreThanTenAndTheNumberOfDesignersisMoreOrTheSameAsProgrammers() {
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(400, manager.countSalary());
        manager.setExperience(3);
        assertEquals(600, manager.countSalary());
        manager.setExperience(6);
        assertEquals(920, manager.countSalary());
    }

    @Test
    void countSalaryIfTeamSizeIsMoreThanTenAndTheNumberOfDesignersIsLessThanProgrammers() {
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(developer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        employees.add(designer1);
        manager.setExperience(1);
        manager.setTeam(employees);
        assertEquals(410, manager.countSalary());
        manager.setExperience(3);
        assertEquals(610, manager.countSalary());
        manager.setExperience(6);
        assertEquals(930, manager.countSalary());
    }
}